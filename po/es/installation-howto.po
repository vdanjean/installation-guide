# debian-installer po translation to Spanish
# Copyright (C) 2004 - 2019 Software in the Public Interest
# This file is distributed under the same license as the debian-installer
# package.
#
# Changes:
#   - Initial translation
#       Javier Fernández-Sanguino, 2004
# 	Ruby Godoy
#
#   - Updates
# 	Camaleón <noelamac@gmail.com>, 2011
# 	Omar Campagne <ocampagne@gmail.com>, 2012 - 2013
# 	Camaleón <noelamac@gmail.com>, 2020
# 	Javier Fernández-Sanguino, 2017, 2019, 2020
#       eulalio <eulalio@disroot.org>, 2022
#       gallegonovato <fran-carro@hotmail.es>, 2023
# 	
# Traductores, si no conocen el formato PO, merece la pena leer la
# documentación de gettext, especialmente las secciones dedicadas a este
# formato, por ejemplo ejecutando:
#       info -n '(gettext)PO Files'
#       info -n '(gettext)Header Entry'
#
# Equipo de traducción al español, por favor lean antes de traducir
# los siguientes documentos:
#
#   - El proyecto de traducción de Debian al español
#     http://www.debian.org/intl/spanish/
#     especialmente las notas y normas de traducción en
#     http://www.debian.org/intl/spanish/notas
#
#   - La guía de traducción de po's de debconf:
#     /usr/share/doc/po-debconf/README-trans
#     o http://www.debian.org/intl/l10n/po-debconf/README-trans
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: debian-boot@lists.debian.org\n"
"POT-Creation-Date: 2022-05-19 22:02+0000\n"
"PO-Revision-Date: 2022-10-22 16:07+0000\n"
"Last-Translator: gallegonovato <fran-carro@hotmail.es>\n"
"Language-Team: Debian l10n Spanish <debian-l10n-spanish@lists.debian.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 4.14.2-dev\n"
"X-POFile-SpellExtra: USB GRUB DVD gz apt cd particionar initrd HDD\n"
"X-POFile-SpellExtra: subsecciones linux particionador NTFS legacy\n"
"X-POFile-SpellExtra: redimensionar support boot FAT gunzip BIOS UEFI CDROM\n"
"X-POFile-SpellExtra: srv Debian PXE Windows PowerMac CD netinst\n"
"X-POFile-SpellExtra: particionado GB reportbug IRC debian hd Macintosh\n"
"X-POFile-SpellExtra: installation pxelinux netboot reports vmlinuz Legacy\n"
"X-POFile-SpellExtra: iso install OpenFirmware OFTC DHCP tftp root\n"

#. Tag: title
#: installation-howto.xml:5
#, no-c-format
msgid "Installation Howto"
msgstr "Instrucciones para la instalación"

#. Tag: para
#: installation-howto.xml:7
#, no-c-format
msgid ""
"This document describes how to install &debian-gnu; &releasename; for the "
"&arch-title; (<quote>&architecture;</quote>) with the new &d-i;. It is a "
"quick walkthrough of the installation process which should contain all the "
"information you will need for most installs. When more information can be "
"useful, we will link to more detailed explanations in other parts of this "
"document."
msgstr ""
"Este documento describe cómo instalar &debian-gnu; &releasename; para &arch-"
"title; (<quote>&architecture;</quote>) con el nuevo &d-i;. Es un paseo "
"rápido por el proceso de instalación que contiene toda la información "
"necesaria para la mayoría de las instalaciones. Enlazaremos con "
"explicaciones más detalladas disponibles en otras partes del documento "
"cuando puedan ser útiles para obtener más información."

#. Tag: title
#: installation-howto.xml:19
#, no-c-format
msgid "Preliminaries"
msgstr "Preliminares"

#. Tag: para
#: installation-howto.xml:20
#, no-c-format
msgid ""
"<phrase condition=\"unofficial-build\"> The debian-installer is still in a "
"beta state. </phrase> If you encounter bugs during your install, please "
"refer to <xref linkend=\"submit-bug\"/> for instructions on how to report "
"them. If you have questions which cannot be answered by this document, "
"please direct them to the debian-boot mailing list (&email-debian-boot-"
"list;) or ask on IRC (#debian-boot on the OFTC network)."
msgstr ""
"<phrase condition=\"unofficial-build\"> El instalador de Debian todavía está "
"en estado beta. </phrase> Si encuentra fallos durante tu instalación, por "
"favor, consulta <xref linkend=\"submit-bug\"/> para instrucciones sobre cómo "
"informar de éstos. Si tiene preguntas que este documento no responda, por "
"favor, escribe a la lista de correo debian-boot (&email-debian-boot-list;) o "
"pregunta en el IRC (#debian-boot en la red OFTC)."

#. Tag: title
#: installation-howto.xml:36
#, no-c-format
msgid "Booting the installer"
msgstr "Arrancar el instalador"

#. Tag: para
#: installation-howto.xml:37
#, no-c-format
msgid ""
"<phrase condition=\"unofficial-build\"> For some quick links to installation "
"images, check out the <ulink url=\"&url-d-i;\"> &d-i; home page</ulink>. </"
"phrase> The debian-cd team provides builds of installation images using &d-"
"i; on the <ulink url=\"&url-debian-cd;\">Debian CD/DVD page</ulink>. For "
"more information on where to get installation images, see <xref linkend="
"\"official-cdrom\"/>."
msgstr ""
"<phrase condition=\"unofficial-build\"> Puedes ver algunos enlaces rápidos a "
"las imágenes de instalación, en la <ulink url=\"&url-d-i;\">página web del "
"&d-i;</ulink>. </phrase> El equipo debian-cd construye imágenes de "
"instalación que utilizan &d-i; y están disponibles en la <ulink url=\"&url-"
"debian-cd;\">página de Debian CD/DVD</ulink>. Para obtener más información "
"sobre dónde obtener imágenes de instalación, ver <xref linkend=\"official-"
"cdrom\"/>."

#. Tag: para
#: installation-howto.xml:47
#, no-c-format
msgid ""
"Some installation methods require other images than those for optical media. "
"<phrase condition=\"unofficial-build\"> The <ulink url=\"&url-d-i;\">&d-i; "
"home page</ulink> has links to other images. </phrase> <xref linkend=\"where-"
"files\"/> explains how to find images on &debian; mirrors."
msgstr ""
"Algunos métodos de instalación requieren imágenes distintas a las de los "
"medios ópticos. <phrase condition=\"unofficial-build\"> La <ulink url=\"&url-"
"d-i;\">página web del &d-i;</ulink> proporciona enlaces a otras imágenes. </"
"phrase> <xref linkend=\"where-files\"/> explica cómo encontrar imágenes en "
"las réplicas de &debian;."

#. Tag: para
#: installation-howto.xml:57
#, no-c-format
msgid ""
"The subsections below will give the details about which images you should "
"get for each possible means of installation."
msgstr ""
"Las siguientes subsecciones dan más detalles sobre las imágenes que deberías "
"conseguir según el método de instalación que vayas a seguir."

#. Tag: title
#: installation-howto.xml:65
#, no-c-format
msgid "Optical disc"
msgstr "Disco óptico"

#. Tag: para
#: installation-howto.xml:67
#, no-c-format
msgid ""
"The netinst CD image is a popular image which can be used to install "
"&releasename; with the &d-i;. This installation method is intended to boot "
"from the image and install additional packages over a network; hence the "
"name <quote>netinst</quote>. The image has the software components needed to "
"run the installer and the base packages to provide a minimal &releasename; "
"system. If you'd rather, you can get a full size CD/DVD image which will not "
"need the network to install. You only need the first image of such set."
msgstr ""
"La imagen de CD «netinst» es una imagen popular que se puede utilizar para "
"instalar &releasename; con el &d-i;. Este método de instalación está "
"diseñado de forma que puede arrancar desde la imagen e instalar los paquetes "
"adicionales que desee a través de la red, de ahí el nombre <quote>netinst</"
"quote>. La imagen incluye los componentes de software necesarios para "
"ejecutar el instalador, y los paquetes base necesarios para proporcionar un "
"sistema mínimo &releasename;. Si lo deseas, puedes obtener una imagen de CD/"
"DVD completa que no necesita disponer de una conexión a la red para poder "
"llevar a cabo la instalación. Sólo necesitas usar la primera imagen del "
"conjunto de CD/DVD de Debian."

#. Tag: para
#: installation-howto.xml:77
#, no-c-format
msgid ""
"Download whichever type you prefer and burn it to an optical disc. <phrase "
"arch=\"any-x86\">To boot the disc, you may need to change your BIOS/UEFI "
"configuration, as explained in <xref linkend=\"bios-setup\"/>.</phrase> "
"<phrase arch=\"powerpc\"> To boot a PowerMac from CD, press the <keycap>c</"
"keycap> key while booting. See <xref linkend=\"boot-cd\"/> for other ways to "
"boot from CD. </phrase>"
msgstr ""
"Descarga la imagen que prefieraa y la grábas en un disco óptico. <phrase "
"arch=\"any-x86\">Para arrancar el disco, es posible que tengas que modificar "
"la configuración de la BIOS, como se detalla en <xref linkend=\"bios-setup\"/"
">.</phrase> <phrase arch=\"powerpc\"> Para arrancar un PowerMac desde el CD, "
"pulsa la tecla <keycap>c</keycap> mientras éste se inicia. Para conocer "
"otras formas de arranque desde CD consulta <xref linkend=\"boot-cd\"/> . </"
"phrase>"

#. Tag: title
#: installation-howto.xml:91
#, no-c-format
msgid "USB memory stick"
msgstr "Dispositivo de memoria USB"

#. Tag: para
#: installation-howto.xml:92
#, no-c-format
msgid ""
"It's also possible to install from removable USB storage devices. For "
"example a USB keychain can make a handy &debian; install medium that you can "
"take with you anywhere."
msgstr ""
"También es posible realizar la instalación desde un dispositivo de "
"almacenamiento USB extraíble. Un llavero USB puede ser un medio de "
"instalación de &debian; bastante útil gracias a que puedes llevarlo consigo "
"a cualquier lugar."

#. Tag: para
#: installation-howto.xml:98
#, no-c-format
msgid ""
"The easiest way to prepare your USB memory stick is to download any Debian "
"CD or DVD image that will fit on it, and write the image directly to the "
"memory stick. Of course this will destroy anything already on the stick. "
"This works because Debian CD/DVD images are <quote>isohybrid</quote> images "
"that can boot both from optical and USB drives."
msgstr ""
"La forma más fácil de preparar tu dispositivo de memoria USB es descargar "
"cualquier imagen de CD o DVD de Debian que tenga un tamaño adecuado para la "
"memoria USB, y después escribir la imagen directamente en la memoria USB. "
"Ten en cuenta que se perderán todos los datos que se encuentren en la "
"memoria USB. Esto es posible debido a que las imágenes de los CD/DVD de "
"Debian son imágenes ISO <quote>híbridas</quote> que se pueden iniciar tanto "
"desde discos ópticos como de memorias USB."

#. Tag: para
#: installation-howto.xml:106
#, no-c-format
msgid ""
"The easiest way to prepare your USB memory stick is to download <filename>hd-"
"media/boot.img.gz</filename>, and use gunzip to extract the 1 GB image from "
"that file. Write this image directly to your memory stick, which must be at "
"least 1 GB in size. Of course this will destroy anything already on the "
"memory stick. Then mount the memory stick, which will now have a FAT "
"filesystem on it. Next, download a &debian; netinst CD image, and copy that "
"file to the memory stick; any filename is ok as long as it ends in <literal>."
"iso</literal>."
msgstr ""
"La forma más fácil de preparar el dispositivo de memoria USB es descargar "
"<filename>hd-media/boot.img.gz</filename> y usar gunzip para extraer la "
"imagen de 1 GB desde este fichero. Escribe esta imagen directamente en el "
"dispositivo de memoria, que debe tener por lo menos 1 GB de tamaño. "
"Obviamente se destruirá todo lo que ya se encuentre en el dispositivo de "
"memoria. A continuación monta el dispositivo de memoria, que ahora tendrá un "
"sistema de ficheros FAT. Por último, descargue una imagen de CD «netinst» de "
"&debian; y copia este fichero en el dispositivo de memoria. Sirve cualquier "
"nombre siempre que termine en <literal>.iso</literal>."

#. Tag: para
#: installation-howto.xml:117
#, no-c-format
msgid ""
"There are other, more flexible ways to set up a memory stick to use the "
"debian-installer, and it's possible to get it to work with smaller memory "
"sticks. For details, see <xref linkend=\"boot-usb-files\"/>."
msgstr ""
"Existen otras formas más flexibles de configurar un dispositivo de memoria "
"para usar el instalador de Debian, y es posible hacerlo funcionar con "
"dispositivos de menor capacidad. Para más detalles, ver <xref linkend=\"boot-"
"usb-files\"/>."

#. Tag: para
#: installation-howto.xml:123
#, no-c-format
msgid ""
"While booting from USB storage is quite common on UEFI systems, this is "
"somewhat different in the older BIOS world. Some BIOSes can boot USB storage "
"directly, and some cannot. You may need to configure your BIOS/UEFI to "
"enable <quote>USB legacy support</quote> or <quote>Legacy support</quote>. "
"The boot device selection menu should show <quote>removable drive</quote> or "
"<quote>USB-HDD</quote> to get it to boot from the USB device. For helpful "
"hints and details, see <xref linkend=\"usb-boot-x86\"/>."
msgstr ""
"El arranque de un almacenamiento USB es muy común en los sistemas UEFI pero "
"es diferente en el caso de BIOS antiguas. Algunas BIOS pueden arrancar "
"directamente desde dispositivos de almacenamiento USB, pero otras no. Puede "
"que necesites configurar la BIOS para activar el <quote>USB legacy support</"
"quote> o <quote>Legacy support</quote>). El menú de selección de arranque de "
"dispositivos debería mostrar un <quote>dispositivos extraíble</quote> o "
"<quote>USB-HDD</quote> para hacerlo arrancar desde el dispositivo USB. "
"Puedes encontrar algunos consejos útiles y obtener más información en <xref "
"linkend=\"usb-boot-x86\"/>."

#. Tag: title
#: installation-howto.xml:148
#, no-c-format
msgid "Booting from network"
msgstr "Arranque desde la red"

#. Tag: para
#: installation-howto.xml:149
#, no-c-format
msgid ""
"It's also possible to boot &d-i; completely from the net. The various "
"methods to netboot depend on your architecture and netboot setup. The files "
"in <filename>netboot/</filename> can be used to netboot &d-i;."
msgstr ""
"También es posible arrancar el &d-i; completamente desde la red. Los "
"diversos métodos de arranque de red dependen de su arquitectura y "
"configuración de arranque desde red. Se pueden usar los ficheros en "
"<filename>netboot/</filename> para arrancar el &d-i; desde la red."

#. Tag: para
#: installation-howto.xml:155
#, no-c-format
msgid ""
"The easiest thing to set up is probably PXE netbooting. Untar the file "
"<filename>netboot/pxeboot.tar.gz</filename> into <filename>/srv/tftp</"
"filename> or wherever is appropriate for your tftp server. Set up your DHCP "
"server to pass filename <filename>pxelinux.0</filename> to clients, and with "
"luck everything will just work. For detailed instructions, see <xref linkend="
"\"install-tftp\"/>."
msgstr ""
"La forma más fácil de configurar el arranque desde red probablemente sea "
"usando PXE. Extrae el archivo <filename>netboot/pxeboot.tar.gz</filename> en "
"<filename>/srv/tftp</filename> o donde sea apropiado para tu servidor tftp. "
"Configura tu servidor DHCP para pasar el fichero <filename>pxelinux.0</"
"filename> a los clientes, y, con suerte, todo funcionará correctamente. Para "
"más instrucciones, ver <xref linkend=\"install-tftp\"/>."

#. Tag: title
#: installation-howto.xml:169
#, no-c-format
msgid "Booting from hard disk"
msgstr "Arrancar desde un disco duro"

#. Tag: para
#: installation-howto.xml:170
#, no-c-format
msgid ""
"It's possible to boot the installer using no removable media, but just an "
"existing hard disk, which can have a different OS on it. Download "
"<filename>hd-media/initrd.gz</filename>, <filename>hd-media/vmlinuz</"
"filename>, and a &debian; CD/DVD image to the top-level directory of the "
"hard disk. Make sure that the image has a filename ending in <literal>.iso</"
"literal>. Now it's just a matter of booting linux with the initrd. <phrase "
"arch=\"x86\"> <xref linkend=\"boot-initrd\"/> explains one way to do it. </"
"phrase>"
msgstr ""
"Es posible arrancar el instalador sin usar medios extraíbles, pero sólo si "
"dispones de un disco duro, que puede tener un sistema operativo diferente. "
"Descarga <filename>hd-media/initrd.gz</filename>, <filename>hd-media/"
"vmlinuz</filename>, y una imagen de CD/DVD de &debian; en el directorio de "
"nivel más alto en el disco duro. Asegúrate de que la imagen tiene un nombre "
"de fichero que termine en <literal>.iso</literal>. Ahora sólo es cuestión de "
"arrancar linux con initrd. <phrase arch=\"x86\"> <xref linkend=\"boot-initrd"
"\"/> explica una forma de hacerlo. </phrase>"

#. Tag: title
#: installation-howto.xml:187
#, no-c-format
msgid "Installation"
msgstr "Instalación"

#. Tag: para
#: installation-howto.xml:188
#, no-c-format
msgid ""
"Once the installer starts, you will be greeted with an initial screen. Press "
"&enterkey; to boot, or read the instructions for other boot methods and "
"parameters (see <xref linkend=\"boot-parms\"/>)."
msgstr ""
"Una vez que se inicie el instalador, aparecerá una pantalla inicial de "
"bienvenida. Pulsa &enterkey; para arrancar, o lee las instrucciones para "
"obtener información de otros métodos y parámetros para la instalación (vea "
"<xref linkend=\"boot-parms\"/>)."

#. Tag: para
#: installation-howto.xml:194
#, no-c-format
msgid ""
"After a while you will be asked to select your language. Use the arrow keys "
"to pick a language and press &enterkey; to continue. Next you'll be asked to "
"select your country, with the choices including countries where your "
"language is spoken. If it's not on the short list, a list of all the "
"countries in the world is available."
msgstr ""
"Después de unos instantes dirá que elijas un idioma. Use las teclas de "
"desplazamiento para elegirlo y pulsa &enterkey; para continuar. Seguidamente "
"solicitará seleccionar su país, las opciones que se muestran incluirán "
"países en donde se habla tu idioma. Si el país no aparece en la lista corta "
"puedes acceder a una lista con todos los países en el mundo."

#. Tag: para
#: installation-howto.xml:202
#, no-c-format
msgid ""
"You may be asked to confirm your keyboard layout. Choose the default unless "
"you know better."
msgstr ""
"Puede que necesites confirmar tu mapa de teclado. Selecciona el propuesto a "
"menos que sepas que no es el adecuado."

#. Tag: para
#: installation-howto.xml:207
#, no-c-format
msgid ""
"Now sit back while debian-installer detects some of your hardware, and loads "
"the rest of the installation image."
msgstr ""
"Ahora siéntate y espere mientras el instalador de Debian detecta el hardware "
"del equipo y carga otros componentes de la imagen de instalación."

#. Tag: para
#: installation-howto.xml:212
#, no-c-format
msgid ""
"Next the installer will try to detect your network hardware and set up "
"networking by DHCP. If you are not on a network or do not have DHCP, you "
"will be given the opportunity to configure the network manually."
msgstr ""
"A continuación el instalador intentará detectar el hardware de red y "
"configurar la red usando DHCP. Podrás configurar la red de forma manual si "
"no estás en una red o no tienes DHCP."

#. Tag: para
#: installation-howto.xml:218
#, no-c-format
msgid ""
"Setting up the network is followed by the creation of user accounts. By "
"default you are asked to provide a password for the <quote>root</quote> "
"(administrator) account and information necessary to create one regular user "
"account. If you do not specify a password for the <quote>root</quote> user, "
"this account will be disabled but the <command>sudo</command> package will "
"be installed later to enable administrative tasks to be carried out on the "
"new system. By default, the first user created on the system will be allowed "
"to use the <command>sudo</command> command to become root."
msgstr ""
"A la configuración de la red le sigue la creación de cuentas de usuario. Por "
"omisión, se te pide que proporciones una contraseña para la cuenta de "
"usuario <quote>root</quote> (administrador) y la información necesaria para "
"crear una cuenta de usuario habitual. Si no se define una contraseña para el "
"usuario <quote>root</quote>, se desactiva esta cuenta y posteriormente se "
"instala el paquete <command>sudo</command> para permitir la realización de "
"tareas de administración en el nuevo sistema. Por omisión, se permitirá al "
"primer usuario creado en el sistema la utilización de la orden "
"<command>sudo</command> para convertirse en usuario root."

#. Tag: para
#: installation-howto.xml:229
#, no-c-format
msgid ""
"The next step is setting up your clock and time zone. The installer will try "
"to contact a time server on the Internet to ensure the clock is set "
"correctly. The time zone is based on the country selected earlier and the "
"installer will only ask to select one if a country has multiple zones."
msgstr ""
"El siguiente paso es fijar el reloj y la zona horaria. El instalador "
"intentará contactar con un servidor de tiempos en Internet para asegurarse "
"que el reloj está bien fijado. La zona horaria se basa en el país "
"seleccionado anteriormente y el instalador sólo pedirá seleccionar una si el "
"país tiene múltiples zonas horarias."

#. Tag: para
#: installation-howto.xml:236
#, no-c-format
msgid ""
"Now it is time to partition your disks. First you will be given the "
"opportunity to automatically partition either an entire drive, or available "
"free space on a drive (see <xref linkend=\"partman-auto\"/>). This is "
"recommended for new users or anyone in a hurry. If you do not want to "
"autopartition, choose <guimenuitem>Manual</guimenuitem> from the menu."
msgstr ""
"Ahora toca particionar los discos. Primero tendrás la oportunidad de "
"particionar automáticamente el disco entero o bien el espacio libre "
"disponible en el disco (ver <xref linkend=\"partman-auto\"/>). Esta opción "
"es la más recomendable para usuarios noveles o alguien con prisa. Utiliza "
"<guimenuitem>Manual</guimenuitem> en el menú si no deseas particionado "
"automático."

#. Tag: para
#: installation-howto.xml:244
#, no-c-format
msgid ""
"If you have an existing DOS or Windows partition that you want to preserve, "
"be very careful with automatic partitioning. If you choose manual "
"partitioning, you can use the installer to resize existing FAT or NTFS "
"partitions to create room for the &debian; install: simply select the "
"partition and specify its new size."
msgstr ""
"Si tienes una partición DOS o Windows que quieras preservar, ten cuidado con "
"el particionado automático. Si elijes particionado manual, puedes usar el "
"instalador para redimensionar particiones FAT o NTFS y dejar espacio para la "
"instalación de &debian; simplemente selecciona la partición y especifica su "
"nuevo tamaño."

#. Tag: para
#: installation-howto.xml:251
#, no-c-format
msgid ""
"On the next screen you will see your partition table, how the partitions "
"will be formatted, and where they will be mounted. Select a partition to "
"modify or delete it. If you did automatic partitioning, you should just be "
"able to choose <guimenuitem>Finish partitioning and write changes to disk</"
"guimenuitem> from the menu to use what it set up. Remember to assign at "
"least one partition for swap space and to mount a partition on <filename>/</"
"filename>. For more detailed information on how to use the partitioner, "
"please refer to <xref linkend=\"di-partition\"/>; the appendix <xref linkend="
"\"partitioning\"/> has more general information about partitioning."
msgstr ""
"En la siguiente pantalla verás la tabla de particiones, cómo se formatearán "
"las particiones, y dónde se montarán. Selecciona una partición para "
"modificarla o eliminarla. Si has efectuado un particionado automático, "
"solamente se te permitirá elegir <guimenuitem>Finalizar particionado y "
"escribir cambios en el disco</guimenuitem> en el menú, para usar lo "
"definido. Recuerda que debes crear por lo menos una partición de intercambio "
"y montar una partición en <filename>/</filename>. Para más detalles de cómo "
"usar el particionado, visita <xref linkend=\"di-partition\"/>; el apéndice "
"<xref linkend=\"partitioning\"/> tiene más información general sobre el "
"particionado."

#. Tag: para
#: installation-howto.xml:264
#, no-c-format
msgid ""
"Now &d-i; formats your partitions and starts to install the base system, "
"which can take a while. That is followed by installing a kernel."
msgstr ""
"Ahora el &d-i; formatea tus particiones y empieza a instalar el sistema "
"base, lo que puede tomar un tiempo. Tras esto se llevará a cabo la "
"instalación del núcleo."

#. Tag: para
#: installation-howto.xml:269
#, no-c-format
msgid ""
"The base system that was installed earlier is a working, but very minimal "
"installation. To make the system more functional the next step allows you to "
"install additional packages by selecting tasks. Before packages can be "
"installed <classname>apt</classname> needs to be configured as that defines "
"from where the packages will be retrieved. The <quote>Standard system</"
"quote> task will be selected by default and should normally be installed. "
"Select the <quote>Desktop environment</quote> task if you would like to have "
"a graphical desktop after the installation. See <xref linkend=\"pkgsel\"/> "
"for additional information about this step."
msgstr ""
"El sistema base que se instala al principio es una instalación funcional, "
"pero mínima. El paso siguiente te permite instalar paquetes adicionales y "
"seleccionar tareas de forma que el sistema instalado sea más operativo. "
"Debes configurar <classname>apt</classname> antes de que se puedan instalar "
"los paquetes, ya que esta configuración define de dónde se obtendrán los "
"paquetes. Por omisión se instala el <quote>Sistema estándar</quote> y es el "
"que generalmente debería estar instalado. Puedes seleccionar el "
"<quote>Entorno de escritorio</quote> si desea tener un entorno gráfico "
"después de la instalación. Para más información sobre este paso consulta "
"<xref linkend=\"pkgsel\"/>."

#. Tag: para
#: installation-howto.xml:281
#, no-c-format
msgid ""
"The last step is to install a boot loader. If the installer detects other "
"operating systems on your computer, it will add them to the boot menu and "
"let you know. <phrase arch=\"any-x86\">By default GRUB will be installed to "
"the UEFI partition/boot record of the primary drive, which is generally a "
"good choice. You'll be given the opportunity to override that choice and "
"install it elsewhere. </phrase>"
msgstr ""
"El último paso es la instalación del gestor de arranque. El instalador "
"añadirá automáticamente al menú de arranque y mostrará un aviso si detecta "
"otros sistemas operativos en el ordenador. <phrase arch=\"any-x86\">GRUB se "
"instala de forma predeterminada en el sector de arranque o partición UEFI "
"del primer disco duro, lo que generalmente es una buena elección. Podrás "
"cambiarlo e instalarlo en otra ubicación si así lo desea.</phrase>"

#. Tag: para
#: installation-howto.xml:291
#, no-c-format
msgid ""
"&d-i; will now tell you that the installation has finished. Remove the cdrom "
"or other boot media and hit &enterkey; to reboot your machine. It should "
"boot up into the newly installed system and allow you to log in. This is "
"explained in <xref linkend=\"boot-new\"/>."
msgstr ""
"Ahora el &d-i; te indicará que la instalación ha finalizado. Retira el CDROM "
"o el medio que hayas utilizado para la instalación y pulsa &enterkey; para "
"reiniciar la máquina. Ésta deberá arrancar en el sistema que acabas de "
"instalar y permitirte autenticarte. Este paso se explica en <xref linkend="
"\"boot-new\"/>."

#. Tag: para
#: installation-howto.xml:298
#, no-c-format
msgid ""
"If you need more information on the install process, see <xref linkend=\"d-i-"
"intro\"/>."
msgstr ""
"Consulta <xref linkend=\"d-i-intro\"/> si necesitas más información sobre el "
"proceso de instalación."

#. Tag: title
#: installation-howto.xml:307
#, no-c-format
msgid "Send us an installation report"
msgstr "Envíanos un informe de la instalación"

#. Tag: para
#: installation-howto.xml:308
#, no-c-format
msgid ""
"If you successfully managed an installation with &d-i;, please take time to "
"provide us with a report. The simplest way to do so is to install the "
"reportbug package (<command>apt install reportbug</command>), configure "
"<classname>reportbug</classname> as explained in <xref linkend=\"mail-"
"outgoing\"/>, and run <command>reportbug installation-reports</command>."
msgstr ""
"Si has realizado una instalación con éxito con el &d-i;, por favor, tómate "
"un momento para enviarnos un informe. La forma más sencilla es instalar el "
"paquete reportbug (<command>apt install reportbug</command>), configurar "
"<classname>reportbug</classname> como se describe en <xref linkend=\"mail-"
"outgoing\"/>, y ejecutar <command>reportbug installation-reports</command>."

#. Tag: para
#: installation-howto.xml:318
#, no-c-format
msgid ""
"If you did not complete the install, you probably found a bug in debian-"
"installer. To improve the installer it is necessary that we know about them, "
"so please take the time to report them. You can use an installation report "
"to report problems; if the install completely fails, see <xref linkend="
"\"problem-report\"/>."
msgstr ""
"Si no logró completar la instalación es posible que haya encontrado un fallo "
"en el instalador de Debian. Para mejorar el instalador es necesario que "
"conozcamos el fallo, así que, por favor, tómate un momento para informar de "
"éste. También puedes usar un informe de instalación para informar sobre los "
"problemas que hayas sufrido. Consulta <xref linkend=\"problem-report\"/> si "
"la instalación falla por completo."

#. Tag: title
#: installation-howto.xml:330
#, no-c-format
msgid "And finally&hellip;"
msgstr "Y finalmente&hellip;"

#. Tag: para
#: installation-howto.xml:331
#, no-c-format
msgid ""
"We hope that your &debian; installation is pleasant and that you find "
"&debian; useful. You might want to read <xref linkend=\"post-install\"/>."
msgstr ""
"Esperamos que tu instalación de &debian; sea satisfactoria y que encuentre "
"útil &debian;. Es posible que ahora quieras leer <xref linkend=\"post-install"
"\"/>."

#~ msgid ""
#~ "Booting Macintosh systems from USB storage devices involves manual use of "
#~ "Open Firmware. For directions, see <xref linkend=\"usb-boot-powerpc\"/>."
#~ msgstr ""
#~ "El arranque de sistemas Macintosh desde dispositivos de almacenamiento "
#~ "USB requiere el uso manual de «OpenFirmware». Para más indicaciones, ver "
#~ "<xref linkend=\"usb-boot-powerpc\"/>."
